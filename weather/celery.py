"""Celery initialization"""

import os

from celery import Celery
from django.conf import settings  # noqa

# set the default Django settings module for the 'celery' program.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "weather.settings")
app = Celery("main")
app.conf.task_acks_late = True

app.config_from_object("django.conf:settings")
app.autodiscover_tasks()

app.conf.beat_schedule = {}
